<?php
/**
 * Plugin Name:       Make Email
 * Plugin URI:        #FIXME
 * Description:       Handle the basics with this plugin.
 * Version:           1.0.0
 * Requires at least: 5
 * Requires PHP:      7
 * Author:            Ashwini Pagar
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       make-email-demo
 * Domain Path:       /languages
 */

define("ME_PLUGIN_BASE_PATH", plugin_dir_path(__FILE__));

 ?>